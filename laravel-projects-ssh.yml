stages:
  - build
  - push-version
  - push-tags
  - deploy-version

build-composer:
  stage: build
  image: vincentcau/laravel-docker:latest
  cache:
    key: ${CI_COMMIT_REF_SLUG}-composer
    paths:
      - vendor/
  script:
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - cp .env.travis .env
    - php artisan key:generate
  artifacts:
    expire_in: 1 month
    paths:
      - vendor/
      - .env
  except:
    - tags

push-production:
  stage: push-version
  image: alpine:latest
  script:
    - apk --no-cache add zip rssh
    - mkdir ~/.ssh
    - cp $SSH_PRIVATE_KEY_PRODUCTION ~/.ssh/id_rsa
    - chmod 700 ~/.ssh/id_rsa
    - zip ../$CI_PIPELINE_IID.zip -r * .[^.]* -x ".git/*"
    - ssh -o BatchMode=yes -o StrictHostKeyChecking=no $SSH_USER_PRODUCTION@$SSH_IP_PRODUCTION mkdir -p $REPOSITORY_PRODUCTION/versions
    - scp -o BatchMode=yes -o StrictHostKeyChecking=no ../$CI_PIPELINE_IID.zip $SSH_USER_PRODUCTION@$SSH_IP_PRODUCTION:$REPOSITORY_PRODUCTION/versions/$CI_PIPELINE_IID.zip
    - ssh -o BatchMode=yes -o StrictHostKeyChecking=no $SSH_USER_PRODUCTION@$SSH_IP_PRODUCTION "unzip -o $REPOSITORY_PRODUCTION/versions/$CI_PIPELINE_IID.zip -d $REPOSITORY_PRODUCTION/$APP_NAME-$CI_PIPELINE_IID && echo $CI_PIPELINE_IID > $REPOSITORY_PRODUCTION/LATEST"
  except:
    - tags

push-staging:
  stage: push-version
  image: alpine:latest
  script:
    - apk --no-cache add zip rssh
    - mkdir ~/.ssh
    - cp $SSH_PRIVATE_KEY_STAGING ~/.ssh/id_rsa
    - chmod 700 ~/.ssh/id_rsa
    - zip ../$CI_PIPELINE_IID.zip -r * .[^.]* -x ".git/*"
    - ssh -o BatchMode=yes -o StrictHostKeyChecking=no $SSH_USER_STAGING@$SSH_IP_STAGING mkdir -p $REPOSITORY_STAGING/versions
    - scp -o BatchMode=yes -o StrictHostKeyChecking=no ../$CI_PIPELINE_IID.zip $SSH_USER_STAGING@$SSH_IP_STAGING:$REPOSITORY_STAGING/versions/$CI_PIPELINE_IID.zip
    - ssh -o BatchMode=yes -o StrictHostKeyChecking=no $SSH_USER_STAGING@$SSH_IP_STAGING "unzip -o $REPOSITORY_STAGING/versions/$CI_PIPELINE_IID.zip -d $REPOSITORY_STAGING/$APP_NAME-$CI_PIPELINE_IID && echo $CI_PIPELINE_IID > $REPOSITORY_STAGING/LATEST"
  except:
    - tags

deploy-staging:
  stage: deploy-version
  image: alpine:latest
  script:
    - apk --no-cache add zip rssh wget
    - mkdir ~/.ssh
    - cp $SSH_PRIVATE_KEY_STAGING ~/.ssh/id_rsa
    - chmod 700 ~/.ssh/id_rsa
    - ssh -o BatchMode=yes -o StrictHostKeyChecking=no $SSH_USER_STAGING@$SSH_IP_STAGING "rm -r $REPOSITORY_STAGING/current && find $REPOSITORY_STAGING -maxdepth 1 -type d -not -name \"$APP_NAME-$CI_PIPELINE_IID\" -name \"$APP_NAME-*\" -exec rm -r {} \; && ln -s $REPOSITORY_STAGING/$APP_NAME-$CI_PIPELINE_IID $REPOSITORY_STAGING/current && mkdir -p $REPOSITORY_STAGING/config && cp -r $REPOSITORY_STAGING/config/. $REPOSITORY_STAGING/current/ && echo $CI_PIPELINE_IID > $REPOSITORY_STAGING/DEPLOY && php $REPOSITORY_STAGING/current/artisan migrate --force"
    - |
      wget --no-check-certificate --quiet \
      --method POST \
      --timeout=0 \
      --header 'Content-Type: application/json' \
      --header 'Accept: application/json' \
      --header 'Content-Type: application/x-www-form-urlencoded' \
      --body-data "token=$PUSH_TOKEN&user=$PUSH_USER&title=$APP_NAME-STAGING&priority=-1&message=Deployed%20version%20$CI_PIPELINE_IID" \
      'https://api.pushover.net/1/messages.json'
  except:
    - tags
  allow_failure: false
  environment:
    name: staging
    url: $CI_ENVIRONMENT_STAGING


deploy-production:
  stage: deploy-version
  image: alpine:latest
  script:
    - apk --no-cache add zip rssh wget
    - mkdir ~/.ssh
    - cp $SSH_PRIVATE_KEY_PRODUCTION ~/.ssh/id_rsa
    - chmod 700 ~/.ssh/id_rsa
    - ssh -o BatchMode=yes -o StrictHostKeyChecking=no $SSH_USER_PRODUCTION@$SSH_IP_PRODUCTION "rm -r $REPOSITORY_PRODUCTION/current && find $REPOSITORY_PRODUCTION -maxdepth 1 -type d -not -name \"$APP_NAME-$CI_PIPELINE_IID\" -name \"$APP_NAME-*\" -exec rm -r {} \; && ln -s $REPOSITORY_PRODUCTION/$APP_NAME-$CI_PIPELINE_IID $REPOSITORY_PRODUCTION/current && mkdir -p $REPOSITORY_PRODUCTION/config && cp -r $REPOSITORY_PRODUCTION/config/. $REPOSITORY_PRODUCTION/current/ && echo $CI_PIPELINE_IID > $REPOSITORY_PRODUCTION/DEPLOY && php $REPOSITORY_PRODUCTION/current/artisan migrate --force"
    - |
      wget --no-check-certificate --quiet \
      --method POST \
      --timeout=0 \
      --header 'Content-Type: application/json' \
      --header 'Accept: application/json' \
      --header 'Content-Type: application/x-www-form-urlencoded' \
      --body-data "token=$PUSH_TOKEN&user=$PUSH_USER&title=$APP_NAME-PRODUCTION&priority=-1&message=Deployed%20version%20$CI_PIPELINE_IID" \
      'https://api.pushover.net/1/messages.json'
  except:
    - tags
  allow_failure: false
  environment:
    name: production
    url: $CI_ENVIRONMENT_PRODUCTION
  when: manual

push-tags:
  stage: push-tags
  image:
    name: alpine/git:latest
    entrypoint: [""]
  script:
    - git config user.email "runner@gitlab.com"
    - git config user.name "Gitlab CI"
    - git tag "t$CI_PIPELINE_IID" -a -m "GitLab release $CI_PIPELINE_IID"
    - git push https://$CI_GITLAB_USER:$CI_GITLAB_TOKEN@gitlab.com/$CI_PROJECT_PATH --tags
  except:
    - tags
  only:
    - master
